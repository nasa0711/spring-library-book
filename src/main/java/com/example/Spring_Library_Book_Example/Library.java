package com.example.Spring_Library_Book_Example;

import com.example.Spring_Library_Book_Example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class Library {
    private int libraryId;
    private String name;
    private String address;
    List<Book> bookList;


    private UserService userService;

    /*@Autowired
    public Library(UserService userService) {
        this.userService = userService;
    }*/

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public String printUserService() {
        return userService.getUserName();
    }


    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public String toString() {
        return "Library{" +
                "libraryId=" + libraryId +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", bookList=" + bookList +
                '}';
    }


}

package com.example.Spring_Library_Book_Example.service.impl;

import com.example.Spring_Library_Book_Example.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {
    @Override
    public String getUserName() {
        return "User logged in sucessfully";
    }
}

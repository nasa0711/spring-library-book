package com.example.Spring_Library_Book_Example.service;

public interface UserService {
    String getUserName();
}

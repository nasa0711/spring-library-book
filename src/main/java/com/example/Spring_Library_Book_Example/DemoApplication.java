package com.example.Spring_Library_Book_Example;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        // load the spring configuration file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("springContext.xml");

        Book book = context.getBean("book", Book.class);

        Library library = context.getBean("library", Library.class);
        library.setLibraryId(1);
        library.setName("Central Library");
        library.setAddress("Tallinn");
        List<Book> books = new ArrayList<>();
        books.add(book);
        library.setBookList(books);
        book.setBookId(1);
        book.setBookName("Harry potter");
        book.setAuthor("Senthil");
        book.setBookType("Fiction");

        System.out.println(library.toString());
        System.out.println(library.printUserService());


        context.close();


    }
}